﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2Zad
{
    class Program
    {
        static void Main(string[] args)
        {
            Product P1 = new Product("Prvi prod", 5);
            Product P2 = new Product("Drugi prof", 10);
            Product P3 = new Product("Treci", 15);

            Box box = new Box();

            box.AddProduct(P1);
            box.AddProduct(P2);
            box.AddProduct(P3);

            IAbstractIterator i1 = box.GetIterator();

            



            for (int i = 0; i < box.Count; i++)
            {
                Console.WriteLine(i1.Current.ToString());
                i1.Next();
            }
        }
    }
}
