﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV6_RPOON_MarkoJović
{
    class Program
    {
        static void Main(string[] args)
        {
            Note note1=new Note("Prvi note","aaa");
            Note note2 = new Note("Drugi note","bbb");
            Note note3 = new Note("Treci note","ccc");

            Notebook N1 = new Notebook();

            N1.AddNote(note1);
            N1.AddNote(note2);
            N1.AddNote(note3);
            N1.RemoveNote(note3);


            IAbstractIterator i1 = N1.GetIterator();

            for (int i=0; i< N1.Count; i++) {
                i1.Current.Show();
                i1.Next();

            }


            


        }
    }
}
