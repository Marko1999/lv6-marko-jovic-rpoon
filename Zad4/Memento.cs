﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zad4
{
    class Memento
    {
        public string OwnerName { get; private set; }
        public string OwnerAdress { get; private set; }
        public decimal Balance { get; private set; }
        


        public Memento(string ownername, string owneradress, decimal balance)
        {
            this.OwnerName = ownername;
            this.OwnerAdress = owneradress;

            this.Balance = balance;
            

        }
    }
}
