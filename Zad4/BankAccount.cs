﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zad4
{
    class BankAccount
    {
        
        private string ownerName;
        private string ownerAdress;
        private decimal balance;
        public BankAccount(string ownerName, string ownerAdress, decimal balance)
        {
            this.ownerName = ownerName;
            this.ownerAdress = ownerAdress;
            this.balance = balance;
        }
        public void ChangeOwnerAdress(string adress)
        {
            this.ownerAdress = adress;
        }
        public void UpdateBalance(decimal amount)
        {
            this.balance += amount;
        }
        public string OwnerName
        {
            get
            {
                return this.ownerName;
            }
        }
        public string OwnerAdress
        {
            get
            {
                return this.ownerAdress;
            }
        }
        public decimal Balance
        {
            get
            {
                return this.balance;
            }
        }
        public void RestoreState(Memento previous)
        {
            this.ownerName = previous.OwnerName;
            this.ownerAdress = previous.OwnerAdress;
            this.balance = previous.Balance;
        }

        public Memento storeData() {
            return new Memento(this.OwnerName, this.OwnerAdress, this.Balance);


        }

        public void restoreData(Memento memento) {
            this.ownerName = memento.OwnerName;
            this.ownerAdress = memento.OwnerAdress;
            this.balance = memento.Balance;



        }

        public override string ToString()
        {
            return this.OwnerName + "\n" + this.OwnerAdress + "\n" + this.Balance;


        }


    }
}
